import assert from 'assert'

import { get } from './_utils.mjs'
import { STRAVA_BASE_URL } from './_consts.mjs'

/** Fetch user activities */
export function activities({ token, after, before } = {}) {
    assert.ok(token, 'Empty access token')

    const url = new URL('/api/v3/athlete/activities', STRAVA_BASE_URL)
    if (after) url.searchParams.set('after', after)

    return get(url.href, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    })
}