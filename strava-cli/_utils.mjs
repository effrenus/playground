import https from 'https'

export function get(url, opts ={}) {
    console.error(url)
    return new Promise((resolve, reject) => {
        const req = https.request(url, {
            method: 'GET',
            headers: Object.assign({}, opts.headers)
        }, (res) => {
            let data = ''
            res.on('data', chunk => data += chunk)
            res.on('end', () => resolve(data))
        })

        req.once('error', reject)

        req.end()
    })
}
