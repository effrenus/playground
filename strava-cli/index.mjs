import { getToken } from './_token.mjs'
import * as api from './_api.mjs'

(async () => {
    const token = await getToken()
    const activities = await api.activities({
        token,
        after: Math.ceil(new Date('2020-07-01').getTime()/1000),
    })

    console.error(activities)
})()
