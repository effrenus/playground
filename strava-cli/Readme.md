## References

* [OAuth](https://tools.ietf.org/html/rfc6749)
* [OAuth Bearer Token](https://tools.ietf.org/html/rfc6750)
* [Strava API](https://developers.strava.com/docs/reference/)