import puppeteer from 'puppeteer'
import http from 'http'
import https from 'https'
import querystring from 'querystring'
import assert from 'assert'

import { STRAVA_BASE_URL } from './_consts.mjs'

async function fetchToken(code) {
    const reqUrl = new URL(`${STRAVA_BASE_URL}/api/v3/oauth/token`)

    return new Promise(resolve => {
        const req = https.request({
            hostname: reqUrl.hostname,
            path: reqUrl.pathname,
            port: 443,
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }, (res) => {
            let data = ''
            res.on('data', chunk => data += chunk)
            res.on('end', () => resolve(data))
        })

        req.end(querystring.stringify({
            client_id: process.env.client_id,
            client_secret: process.env.client_secret,
            grant_type: 'authorization_code',
            code,
        }))
    })
}

async function setupServer() {
    const server = http.createServer()
    return new Promise(resolve => server.listen(null, process.env.HOST || '127.0.0.1', () => resolve(server)))
}

function createOAuthUrl({ redir_uri }) {
    const url = new URL('oauth/authorize', STRAVA_BASE_URL)
    url.searchParams.set('response_type', 'code')
    url.searchParams.set('scope', 'activity:read,read')
    url.searchParams.set('client_id', process.env.client_id)
    url.searchParams.set('redirect_uri', redir_uri)
    return url.href
}

/** Getting access token from Strava authorizing server. */
export async function getToken() {
    assert.ok(process.env.client_id, 'Need to setup `client_id`')
    assert.ok(process.env.client_secret, 'Need to setup `client_secret`')

    // TODO(effrenus): Cache result.
    const browser = await puppeteer.launch({
        headless: false,
        defaultViewport: {
            width: 900,
            height: 900
        },
        args: ['--window-size=900,900']
    })

    browser.once('disconnected', _ => {
        console.error('Browser unexpectedly disconnected')
        process.exit(1)
    })
    
    const server = await setupServer()
    const serverBaseUrl = `http://${server.address().address}:${server.address().port}`

    const page = await browser.newPage()
    await page.goto(createOAuthUrl({ redir_uri: serverBaseUrl }))

    return new Promise(resolve => 
        server.once('request', async (req, res) => {
            const url = new URL(req.url, serverBaseUrl)
            
            res.end('OK')
            server.close()
            browser.close()

            resolve(JSON.parse(await fetchToken(url.searchParams.get('code'))).access_token)
        })
    )
}
