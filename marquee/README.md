## Original

![marquee element](./sample.png)


## CSS

```css
html,body {
  width: 100%;
  height: 100%;
  background: #ccc;
  margin: 0;
  padding: 0;
}

.elm {
  width: 100%;
  height: 60px;
  margin: auto;
  background-image: url(…);
  background-position: 0 0;
  animation: marquee 90s infinite alternate linear;
}

@keyframes marquee {
  0% {
    background-position: 0;
  }
  100% {
    background-position: -10000px;
  }
}
```

https://jsbin.com/xogijudufi/edit?css,output

## Javascript

Canvas
