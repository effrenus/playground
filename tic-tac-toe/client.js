const debug = (...args) => console.log(...args)

const ws = new WebSocket('ws://192.168.0.101:3002/')

ws.onerror = (err) => {
    console.error('socket error: %s', err)
    reset({ title: 'Cannot connect to server' })
}
ws.onclose = (evt) => {
    console.warn('socket closed. %s', evt.reason || 'unknown reason')
    reset({ title: 'Disconnected from server 🔌' })
}
ws.onmessage = handleData
ws.onopen = () => {
    debug('socket opened')
    initGame()
}

function send(data) {
    ws.send(JSON.stringify(
        data
    ))
}

const gameState = {
    isMoveEnabled: false,
    playerChar: null,
}

function initGame() {
    debug('initializing')
    send({ type: 'join' })
}

function handleData(data) {
    debug('received data', data.data)
    const action = JSON.parse(data.data)

    switch (action.type) {
        case 'move':
            gameState.isMoveEnabled = true
            setStatus('Your turn')
            break
        case 'start':
            gameState.isMoveEnabled = action.payload.isActive
            gameState.playerChar = action.payload.char
            document.body.classList.remove('_searching', '_starting')
            document.body.classList.add('_playing')
            reset({ title: gameState.isMoveEnabled ? 'Your turn' : 'Waiting opponent move…'})
            break
        case 'update':
            document.querySelector(`.board .cell:nth-child(${+action.payload+1})`).textContent = gameState.playerChar === 'X' ? 'O' : 'X'
            document.querySelector(`.board .cell:nth-child(${+action.payload+1})`).classList.add('active')
            break
        case 'end':
            setStatus(action.payload === 'tie' ? 'Tie 🤷🏻‍♂️' : action.payload === 'win' ? 'You are winner 👑' : 'Better luck next time ☘️')
            setTimeout(reset, 3000)
            break
    }
}

function setStatus(txt) {
    document.querySelector('.notice').textContent = txt
}

function reset({ title } = {}) {
    setStatus(title ?? 'Start Tic-Tac-Toe game')
    ;[...document.querySelectorAll('.board .cell')].forEach(elm => {
        elm.classList.remove('active')
        elm.textContent = ''
    })
    document.body.classList.remove('_searching', '_playing')
    document.body.classList.add('_starting')
}

document.querySelector('.board').addEventListener('click', (evt) => {
    if (!gameState.isMoveEnabled) return
    if (evt.target.textContent.trim() !== '') return

    evt.target.textContent = gameState.playerChar
    gameState.isMoveEnabled = false

    send({
        type: 'update',
        payload: evt.target.dataset.pos
    })

    setStatus('Waiting opponent move…')
})

document.querySelector('#find-btn').addEventListener('click', () => {
    debug('searching for opponent')

    send({ type: 'search' })
    
    reset({ title: 'Searching for opponent…' })
    document.body.classList.add('_searching')
})

reset()
