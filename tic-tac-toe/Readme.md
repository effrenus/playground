# Multiplayer Tic-Tac-Toe

## [Play](http://cedar-absorbing-soul.glitch.me/index.html)

## Specifications

* Websocket – [rfc6455](https://tools.ietf.org/html/rfc6455)
* Websocket Compression Extensions – [rfc7692](https://tools.ietf.org/html/rfc7692) (Not implemented)
* Websocket Multiplexing - [Draft v11 (2014)](https://tools.ietf.org/html/draft-ietf-hybi-websocket-multiplexing-11)
* Websocket API - [MDN](https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API)

## Notes

* Firefox 79 support masked frames from server and doesn't close connection.

Specs [says](https://tools.ietf.org/html/rfc6455#section-5.1):

> The server MUST close the connection upon receiving a frame that is not masked.
> In this case, a server MAY send a Close frame with a status code of 1002 (protocol error) as defined in Section 7.4.1.
> A server MUST NOT mask any frames that it sends to the client.
> A client MUST close a connection if it detects a masked frame.

## Additional materials

* [Awesome WebSockets](https://github.com/facundofarias/awesome-websockets)
* [WAMP protocol](https://wamp-proto.org/spec.html)
* [The Unsolved Load Balancing Problem of WebSockets](https://movingfulcrum.com/the-unsolved-load-balancing-problem-of-websockets/)