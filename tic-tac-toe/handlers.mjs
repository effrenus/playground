import crypto from 'crypto'

import Frame from './frame.mjs'
import FrameEncoder from './encoder.mjs'
import createDebug from './debug.mjs'
import Game from './game.mjs'
import Player from './player.mjs'
import { shuffleInPlace } from './utils.mjs'

const debug = createDebug('websocket:handler')

export {
    handleUpgrade,
    handleControlFrame,
    handleDataFrame,
    handleClose,
}

function send(sock, data) {
    const frame = new Frame
    frame.payload = JSON.stringify(data)

    sock.write(FrameEncoder.encode(frame))
}

function handleUpgrade(sock, reqBuf) {
    const GUID = '258EAFA5-E914-47DA-95CA-C5AB0DC85B11'

    const headers = new Map(reqBuf
        .toString()
        .split('\n')
        .map(s => s.split(/:\s*/).map(s => s.trim()))
    )

    if (!headers.has('Sec-WebSocket-Key')) {
        debug('invalid headers: cannot find `Sec-WebSocket-Key` header')
        sock.end()
        return
    }

    debug('upgrading connection')

    const key = headers.get('Sec-WebSocket-Key')
    const hash = crypto.createHash('sha1').update(`${key}${GUID}`).digest('base64')

    sock.write([
        'HTTP/1.1 101 Switching Protocols',
        'Connection: Upgrade',
        'Upgrade: websocket',
        `Sec-WebSocket-Accept: ${hash}`
    ].join('\r\n') + '\r\n\r\n')
}

function handleControlFrame(sock, frame) {
    debug('handling control frame')

    if (frame.isClosing) handleClose(sock)
}

const players = new Map // TODO(effrenus): WeakMap?

function handleDataFrame(sock, frame) {
    debug('handling data frame')
    debug(`data frame payload: <${frame.payload}>`)

    const action = JSON.parse(frame.payload)

    debug('action', action)

    switch (action.type) {
        case 'join': 
            players.set(sock, new Player(sock))
            break
        case 'search': {
            const shuffledWaitPlayers = shuffleInPlace([...players]).filter(([sock_, player]) => sock !== sock_ && !player.isPlaying() )
            if (shuffledWaitPlayers.length) {
                const [_, player] = shuffledWaitPlayers[0]

                const game = new Game(players.get(sock), player)
                game.on('action', (player, action) => send(player.sock, action))
                game.start()
            }
            break
        }
        case 'update': {
            const player = players.get(sock)
            player.game.update(+action.payload)
            break
        }
    }
}

function handleClose(sock) {
    const player = players.get(sock)

    if (!player) {
        debug('cannot find player for closed socket')
        return
    }

    debug('deleting player binded to closed socket')
    players.delete(sock)
}
