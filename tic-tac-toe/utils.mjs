const rand = (from, to) => Math.ceil(Math.random() * (to - from)) + from

export function shuffleInPlace(arr) {
    const len = arr.length
    for (let i = 0; i < len; ++i) {
        const j = rand(i, len-1)
        if (i === j) continue
        [arr[i], arr[j]] [arr[j], arr[i]]
    }
    return arr
}