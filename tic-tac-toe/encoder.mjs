import crypto from 'crypto'

import createDebug from './debug.mjs'

const debug = createDebug('websocket:encoder')

const maskPayload = (payload, mask) => {
    payload = new Uint8Array(payload)
    mask = new Uint8Array(mask)
    for (let i = 0; i < payload.length; ++i) {
        payload[i] ^=  mask[i%4]
    }
    return payload
}

class FrameEncoder {
    static encode(frame) {
        let frameSizeBytes = 1 + 1
        if (frame.mask) frameSizeBytes += 4

        let headBuf = Buffer.alloc(frameSizeBytes)
        headBuf.writeUInt8(
            (1 << 7) | 0x1
        )
        headBuf.writeUInt8(
            (frame.mask << 7) | frame.payload?.length ?? 0,
            1
        )

        let payload = Buffer.from(frame.payload)

        if (frame.mask) {
            const maskBuf = crypto.randomBytes(4)
            payload = maskPayload(payload, maskBuf)

            headBuf.writeUInt32BE(maskBuf.readUInt32BE(), 2)
            
            debug('mask> ', mask)
        }

        return Buffer.concat([headBuf, Buffer.from(payload)])
    }
}

export default FrameEncoder
