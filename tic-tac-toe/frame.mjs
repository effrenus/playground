class Frame {
    fin = 1
    rsv1 = 0
    rsv2 = 0
    rsv3 = 0
    mask = 0

    static OPCODES = new Map([
        ['CONT', 0],
        ['TEXT', 0x1],
        ['BINARY', 0x2],
        ['CLOSE', 0x8],
        ['PING', 0x9],
        ['PONG', 0xA],
    ])

    isFinal() {
        return this.fin === 1
    }

    isControl() {
        return this.opcode !== Frame.OPCODES.get('TEXT') &&
        this.opcode !== Frame.OPCODES.get('BINARY')
    }

    isClosing() {
        return this.opcode === Frame.OPCODES.get('CLOSE')
    }
}

export default Frame
