class Player {
    #sock = null

    game = null

    constructor(sock) {
        this.#sock = sock
    }

    get sock() {
        return this.#sock
    }

    isPlaying() {
        return Boolean(this.game)
    }
}

export default Player
