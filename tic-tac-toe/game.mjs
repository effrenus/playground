import EventEmitter from 'events'

const chs = ['X', 'O']

class Game extends EventEmitter {
    static STATUSES = {
        END_WIN: 1,
        END_TIE: 0,
        
    }

    #players = null
    #activePlayer = null

    board = Array(9).fill(null)

    constructor(player1, player2) {
        super()

        this.#players = [player1, player2]
        player1.game = this
        player2.game = this
    }

    start() {
        this.#activePlayer = Math.random() < 0.5 ? 0 : 1
        
        this.emitAction(this.#activePlayer, { type: 'start', payload: { isActive: true, char: chs[this.#activePlayer] } })
        this.emitAction((this.#activePlayer + 1) % 2, { type: 'start', payload: { isActive: false, char: chs[(this.#activePlayer + 1) % 2] } })
    }

    emitAction(playerId, ...args) {
        this.emit('action', this.#players[playerId], ...args)
    }

    playerByIdx(idx) {
        return this.#players[idx]
    }

    getStatus() {
        for (let i = 0; i < 3; ++i) {
            const row = this.board.slice(i*3, i*3+3).join``
            if (row === 'XXX' || row === 'OOO') return Game.STATUSES.END_WIN
        }

        for (let i = 0; i < 3; ++i) {
            let col = ''
            for (let j = 0; j < 3; ++j) {
                col += this.board[i+j*3]
            }
            if (col === 'XXX' || col === 'OOO') return Game.STATUSES.END_WIN
        }

        return this.board.every(v => Boolean(v)) ? Game.STATUSES.END_TIE : -1
    }

    update(pos) {
        this.board[pos] = chs[this.#activePlayer]

        const nextPlayer = (this.#activePlayer+1) % 2
        this.emitAction(nextPlayer, {
            type: 'update',
            payload: pos
        })

        const status = this.getStatus()
        if (status !== -1) {
            this.emitAction(this.#activePlayer, {
                type: 'end',
                payload: status !== 0 ? 'win' : 'tie'
            })
            this.emitAction(nextPlayer, {
                type: 'end',
                payload: status !== 0 ? 'lose' : 'tie'
            })
            this.#players.forEach(player => player.game = null)
            return
        }

        this.#activePlayer = nextPlayer

        this.emitAction(this.#activePlayer, {
            type: 'move'
        })
    }
}

export default Game
