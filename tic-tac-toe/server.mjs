import net from 'net'

import createDebug from './debug.mjs'
import FrameDecoder from './decoder.mjs'
import {
    handleUpgrade,
    handleControlFrame,
    handleDataFrame,
    handleClose
} from './handlers.mjs'

const debug = createDebug('websocket')

const server = new net.Server(async (sock) => {
    debug('connection')

    sock.on('data', reqData => {
        if (reqData.includes('GET')) {
            return handleUpgrade(sock, reqData)
        }
    
        const frame = FrameDecoder.decode(reqData)

        if (frame.isControl()) {
            debug('control frame received')
            handleControlFrame(sock, frame)
        } else {
            debug('data frame received')
            handleDataFrame(sock, frame)
        }
    })

    sock.on('error', err => {
        debug('socket error', err)
        handleClose(sock)
    })
    sock.on('end', _ => {
        debug('socket closed')
        handleClose(sock)
    })
})

server.listen(
    process.env.SOCKET_PORT ?? 3002,
    process.env.SOCKET_HOST ?? '127.0.0.1',
    () => debug(`server listening: ${server.address().address} ${server.address().port}`)
)
