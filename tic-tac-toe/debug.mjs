export default function createDebug(namespace) {
    return (...args) => console.error(`${namespace}:`, ...args)
}