import Frame from './frame.mjs'

class FrameDecoder {
    static decode(buf) {
        const frame = new Frame

        frame.opcode = buf.readUInt8() & 0xF
        frame.rsv3 = buf.readUInt8() >> 4 & 0x1
        frame.rsv2 = buf.readUInt8() >> 5 & 0x1
        frame.rsv1 = buf.readUInt8() >> 6 & 0x1
        frame.fin = buf.readUInt8() >> 7

        let maskKeyOffset = 2
        {
            const byte = buf.readUInt8(1)
            frame.mask = (byte >> 7) & 0x1

            let payloadLen = byte & 0x7F

            if (payloadLen === 0x7E) {
                payloadLen = buf.readUInt16BE(2)
                maskKeyOffset = 4
            } else if (payloadLen === 0x7F) {
                payloadLen = buf.readBigInt64BE(2)
                maskKeyOffset = 10
            }
            frame.payloadLen = payloadLen
        }

        if (frame.mask) {
            const mask = [
                buf.readUInt8(maskKeyOffset),
                buf.readUInt8(maskKeyOffset + 1),
                buf.readUInt8(maskKeyOffset + 2),
                buf.readUInt8(maskKeyOffset + 3),
            ]

            frame.headerSize = maskKeyOffset + 4

            let decodedData = ''
            for (let i = 0; i < frame.payloadLen; ++i) {
                decodedData += String.fromCharCode(
                    buf.readUInt8(frame.headerSize + i) ^ mask[i%4]
                )
            }

            frame.payload = decodedData
        }

        return frame
    }
}

export default FrameDecoder
