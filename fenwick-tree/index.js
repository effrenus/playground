const LSB = i => i&-i

class FenwickTree {
  constructor(originalArr) {
    this.values = [0].concat(originalArr) // Create shallow copy.
    this._construct()
  }
  
  get size() {
    return this.values.length
  }
  
  _construct() {
    for (let i = 1; i < this.size; ++i) {
      const parentIdx = i + LSB(i)
      if (parentIdx < this.size)
        this.values[parentIdx] += this.values[i]
    }
  }
  
  _get(idx) {
    let sum = 0
    while (idx) {
      sum += this.values[idx]
      idx ^= LSB(idx)
    }
    return sum
  }
  
  query(from, to) {
    return (from > 0 ? this._get(from) : 0) + this._get(to+1)
  }
  
  update(idx, value) {
    idx += 1
    const diff = value - this.values[idx]
    while (idx < this.size) {
      this.values[idx] += diff
      idx += LSB(idx)
    }
  }
}

const tree = new FenwickTree([1,1,1,1])

console.assert(tree.query(0,1) == 2)
console.assert(tree.query(0,3) == 4)
