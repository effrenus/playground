// With simple grid based optimizations.

class MetaThing {
    constructor({ x, y, r = 20, speed = { x: 3, y: 3 }, dir = { x: 1, y: 1 } }) {
        this.x = x
        this.y = y
        this.r = r
        this.speed = speed
        this.dir = dir
    }
    
    update() {
        this.x += this.speed.x*this.dir.x
        this.y += this.speed.y*this.dir.y
        
        if (this.x <= 0 || this.x >= SCENE_DIM.width) this.dir.x *= -1
        if (this.y <= 0 || this.y >= SCENE_DIM.height) this.dir.y *= -1
    }
}

const SCENE_DIM = { width: document.body.offsetWidth, height: document.body.offsetHeight }
const GRID_SIZE = 30
const THRESHOLD_MIN = 0.99

function render({ ctx, things }) {
    const { width: sceneW, height: sceneH } = SCENE_DIM

    const gradient = ctx.createLinearGradient(0, 0, sceneW, sceneH)
    gradient.addColorStop(0, '#0f0c29')
    gradient.addColorStop(.5, '#302b63')
    gradient.addColorStop(1, '#24243e')
    
    ctx.fillStyle = gradient
    ctx.fillRect(0, 0, sceneW, sceneH)
    
    const computeM = (x, y) => {
        let m = 0
        for (const thing of things) {
            m += thing.r/Math.sqrt((x-thing.x)**2 + (y-thing.y)**2)
            if (m >= 0.99) break
        }
        return m
    }
    
    for (let j = 0; j < sceneH; j += GRID_SIZE) {
        for (let i = 0; i < sceneW; i += GRID_SIZE) {
            if (![[0,0],[GRID_SIZE,0],[GRID_SIZE,GRID_SIZE],[0,GRID_SIZE]].some(([dx, dy]) => computeM(i+dx, j+dy) >= THRESHOLD_MIN)) continue

            renderGridCell({
                fromX: i, toX: i+GRID_SIZE,
                fromY: j, toY: j+GRID_SIZE,
            })
        }
    }
    
    function renderGridCell({ fromX, toX, fromY, toY }) {
        ctx.fillStyle = `rgba(72, 112, 224, 1)`
        
        if ([[0,0],[GRID_SIZE,0],[GRID_SIZE,GRID_SIZE],[0,GRID_SIZE]].every(([dx, dy]) => computeM(fromX+dx, fromY+dy) >= THRESHOLD_MIN)) {
            ctx.fillRect(fromX, fromY, GRID_SIZE, GRID_SIZE)
            return
        }
        
        for (let x = fromX; x <= toX; ++x) {
            for (let y = fromY; y < toY; ++y) {
                if (computeM(x,y) < THRESHOLD_MIN) continue
                ctx.fillRect(x, y, 1, 1)
            }
        }
    }
    
    for (const thing of things) thing.update()
    
    requestAnimationFrame(() => render({ ctx, things }))
}

;(() => {
    const canva = document.createElement('canvas')
    canva.width = SCENE_DIM.width
    canva.height = SCENE_DIM.height
    document.body.appendChild(canva)
    
    const ctx = canva.getContext('2d')
    const things = [
        new MetaThing({ x: 100, y: 100, r: 200, speed: { x: 1.8, y: 1.6 } }),
        // new MetaThing({ x: 120, y: 180 }),
        // new MetaThing({ x: 100, y: 380 }),
        // new MetaThing({ x: 10, y: 10, r: 70 }),
        new MetaThing({ x: 220, y: 120, r: 250 })
    ]
    
    render({ ctx, things })
})()