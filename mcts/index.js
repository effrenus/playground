const rand = (min, max) => (Math.round(Math.random() * (max - min)) + min)
const choice = (arr) => arr[rand(0, arr.length-1)]

class Stat {
  constructor(value = 0, visits = 0) {
    this.value = value
    this.visits = visits
  }

  toString() {
    return `Stat(value=${this.value},visits=${this.visits})`
  }
}

class MCTS {
  constructor(board, opts = {}) {
    this.board = board
    this.history = [board.constructor.getInitState(this.board)]
    this.stats = new Map

    this.max_depth = 0

    this.simulation_time = opts.simulation_time || 4000
    this.max_actions = opts.max_actions || 2000

    this.C = opts.C || 1.4
  }

  get_action() {
    this.data = {}
    this.max_depth = 0
    this.stats.clear()

    let state = this.history[this.history.length-1]
    let player = this.board.current_player(state)
    let legal = this.board.legal_actions(state)
    console.log(legal)

    if (!legal) return { type: 'action', message: null }
    if (legal.length == 1) return { type: 'action', message: this.board.to_json_action(legal[0]) }

    let games = 0
    const begin = Date.now()
    while (Date.now() - begin < this.simulation_time) {
      this.run_simulation()
      games += 1
    }

    console.log(`games player: ${games}`)

    this.data.actions = this.calculate_action_values(this.history, player, legal)

    return {
      type: 'action',
      message: this.board.to_json_action(this.data.actions[0].action)
    }
  }

  run_simulation() {
    const [C, stats] = [this.C, this.stats]

    const visited_states = []
    const history = this.history.slice()
    let state = history[history.length-1]

    let expand = true
    for (let i = 0; i < this.max_actions; ++i) {
      const legal = this.board.legal_actions(state)
      let actions_states = legal.map(action => [action, this.board.next_state(history, action)])

      if (expand && !actions_states.every(([_,state]) => stats.has(k(state)))) {
          actions_states.forEach(([_, state]) => {
            if (stats.has(k(state))) return
            stats.set(k(state), new Stat)
          })
          expand = false
      }

      if (expand) {
        actions_states = actions_states.map(([action, state]) => [action, state, stats.get(k(state))])
        const log_total = Math.log(actions_states.reduce((s, v) => v[2].visits + s, 0) || 1)
        const values = actions_states.map(([action, state, stat]) => [action, state, stat.value/(stat.visits||1)+C*Math.sqrt(log_total/(stat.visits||1))])
        const max_value = Math.max(...values.map(v => v[2]))
        actions_states = actions_states.filter(v => v[2] != max_value)
      }

      const [action, nstate] = choice(actions_states)
      state = nstate
      visited_states.push(nstate)
      history.push(nstate)

      if (this.board.is_ended(nstate)) break
    }

    const end_values = this.end_values(state)
    for (const state of visited_states) {
      if (!stats.has(k(state))) continue
      if (!end_values) continue
      const S = stats.get(k(state))
      S.visits += 1
      S.value += end_values[this.board.previous_player(state)]
    }
  }
}

class MCRSWin extends MCTS {
  constructor (board, opts) {
    super(board, opts)
    this.end_values = board.win_values.bind(board)
  }

  calculate_action_values (history, player, legal) {
    const actions_states = legal.map(a => [a, this.board.next_state(history, a)])
    // console.log(this.stats)

    return actions_states.map(([a, s]) => {
      return {
        action: a,
        percent: 100*this.stats.get(k(s)).value / (this.stats.get(k(s)).visits||1),
        wins: this.stats.get(k(s)).value,
        plays: this.stats.get(k(s)).visits
      }
    }).sort((a,b) => b.percent == a.percent ? b.plays-a.plays : b.percent - a.percent)
  }
}
