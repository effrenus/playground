const k = (state) => `board:${state.board.map(c=>c.join`,`).join`|`},player:${state.player}`

class ConnectFour {
  static getInitState(board) {
    return {
      // board: Array(board.w).fill().map(_ => []),
      board: [
        [2,2,2,1,2,2],
        [1,1,2,2,2,1],
        [2,1,1,1,2,1],
        [1,2,2,2,1,1],
        [1,1,1,2,1,2],
        [2,2],
        [1,1,1,2,1,2],
      ],
      player: 2
    }
  }

  constructor(w = 7, h = 6) {
    this.w = w
    this.h = h
  }

  previous_player({ player }) {
    return 3 - player
  }

  current_player({ player } = {}) {
    if (!player) return 1
    return 3 - player
  }

  to_json_action(action) {
    return JSON.stringify(action)
  }

  legal_actions(state = {}) {
    const { board, player } = state
    if (!board) {
      return Array(this.w).fill().map((_, i) => [i, 1])
    }
    return board.map((col, i) => [i, col]).filter((v, i) => v[1].length < this.h).map(v => [v[0], 3 - player])
  }

  next_state(history, [col, player]) {
    const { board } = history[history.length-1] || ConnectFour.getInitState(this.w, this.h)
    const newState = {
      board: board.map(c => c.slice()),
      player
    }
    if (!newState.board[col]) newState.board[col] = []
    newState.board[col].push(player)
    return newState
  }

  is_ended({ board }) {
    if (board.every(col => col.length == this.h)) return true
    if (board.some(col => /0{4}|1{4}/.test(col.join``))) {
      // console.log(board.map(r=>r.join``).join`\n`)
      // console.log('~~~~~~~~~~~~~')
      return true
    }
    for (let i = 0; i < this.h; ++i) {
      let r = ''
      for (let j = 0; j < this.w; ++j) {
        r += board[j][i] || '_'
      }
      if (/0{4}|1{4}/.test(r)) return true
    }

    return false
  }

  win_values(state) {
    if (!this.is_ended(state)) return
    const { player, board } = state
    if (board.some(col => /1{4}/.test(col.join``))) return {1: 0, 2: 1}
    if (board.some(col => /2{4}/.test(col.join``))) return {1: 1, 2: 0}

    for (let i = 0; i < this.h; ++i) {
      let r = ''
      for (let j = 0; j < this.w; ++j) {
        r += board[j][i] || '_'
      }
      if (/1{4}/.test(r)) return {1: 0, 2: 1}
      if (/2{4}/.test(r)) return {1: 1, 2: 0}
    }

    return {1: 0.5, 2: 0.5}
  }
}
