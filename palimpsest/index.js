#!/usr/bin/env node

/** Brute-force, random 😼 */

const assert = require('assert')
const { createCanvas, loadImage } = require('canvas')
const fetch = require('node-fetch')

class BrokenPixelError extends Error {}

const rgbToHex = (...rgb) => rgb.map(v => v.toString(16).padStart(2,'0')).join``.toUpperCase()
const rand = (min, max) => Math.floor(Math.random() * (max - min + 1) + min)
const log = (...args) => console.error(...args)

async function postPaintData(data) {
  const res = await fetch(
    'http://bigcanvasdemo.com/draw.php?submit=1',
    {
      method: 'POST',
      body: data,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Content-Length': data.length,
        'Referer': 'http://bigcanvasdemo.com/draw.php?x=0&y=0',
        'X-Requested-With': 'XMLHttpRequest',
        'User-Agent': 'Banksy/1',
      }
    }
  )

  return res.buffer()
}

const tryFillBigPixel = async (ctx, [x, y], opts = {}) => {
  log(`filling x=${x} y=${y}`)

  const defaults = {
    maxThreshold: 0.15,
    thresholdStep: 0.02,
    maxAttempts: 50,
    startX: 1,
    startY: 1
  }
  opts = { ...defaults, ...opts }

  let attemptsRemain = opts.maxAttempts
  let currentThreshold = 0.05

  while (attemptsRemain--) {
    let body = `x=${x}&y=${y}`;
      
    for (let i = opts.startX; i < DIMENSION; i += 1) {
      for (let j = opts.startY; j < DIMENSION; j += 1) {
        if (Math.random() < currentThreshold) continue

        const [r,g,b] = ctx.getImageData(x*DIMENSION+i, y*DIMENSION+j, 1, 1).data
        body += `&data[${i},${j}]=#${rgbToHex(r, g, b)}`
      }
    }

    const ans = await postPaintData(body)

    if (ans.includes('index.php')) return
    
    currentThreshold = Math.min(currentThreshold + opts.thresholdStep, opts.maxThreshold)
  }

  throw new BrokenPixelError()
}

async function paintOnRemoteCanvas(ctx, pixels) {
  const brokenPixels = []

  for (const [x, y] of pixels) {
    try {
      await tryFillBigPixel(
        ctx,
        [x, y],
        {
          maxAttempts: rand(5, 20),
          startX: rand(1, 3),
          startY: rand(1, 3)
        }
      )
    } catch (err) {
      if (!(err instanceof BrokenPixelError)) throw err
      brokenPixels.push([x, y])
    }
  }

  return brokenPixels
}

let DIMENSION = 25
let PIXELSIZE = 2
let REPEATSX = 20
let REPEATSY = 15

;(async function main(pathToImage) {
  assert.ok(pathToImage, 'Need to pass path to image')

  let canvasWidth = DIMENSION * REPEATSX * PIXELSIZE
  let canvasHeight = DIMENSION * REPEATSY * PIXELSIZE

  const maxX = Math.floor(canvasWidth/(DIMENSION*PIXELSIZE))
  const maxY = Math.floor(canvasHeight/(DIMENSION*PIXELSIZE))

  const canvas = createCanvas(canvasWidth, canvasHeight)
  const ctx = canvas.getContext('2d')

  const image = await loadImage(pathToImage)
  ctx.drawImage(image, 0, 0, canvasWidth/2, canvasHeight/2)

  let pixelsNeedToPaint = []
  for (let x = 0; x < maxX; ++x) {
    for (let y = 0; y < maxY; ++y) {
      pixelsNeedToPaint.push([x, y])
    }
  }

  try {
    while (pixelsNeedToPaint.length) pixelsNeedToPaint = await paintOnRemoteCanvas(ctx, pixelsNeedToPaint)
  } catch (err) {
    log(err)
    process.exit(1)
  }

})(process.argv[2])
