// ==UserScript==
// @name         MediaSession API support for YMusic
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Add basic MediaSession support
// @author       You
// @match        https://music.yandex.ru/home
// @match        https://music.yandex.ru/non-music
// @match        https://music.yandex.ru/kids
// @match        https://music.yandex.ru/radio
// @icon         https://www.google.com/s2/favicons?domain=yandex.ru
// @grant        none
// ==/UserScript==

(function () {
  const UI_SELECTORS = {
    play: ".player-controls__btn_play",
    next: ".player-controls__btn_next",
    prev: ".player-controls__btn_prev",
    controls_container: ".player-controls__track-container",
    cover_image: ".track .entity-cover__image",
    track_title: ".track .track__title",
    track_artist: ".track .track__artists",
  };

  let curTrackName = null;

  setTimeout(setup, 3000);

  function setup() {
    navigator.mediaSession.setActionHandler("play", () => {
      console.log("[mediasession] handling play");
      if (!isPlaying()) {
        clickOnUI(UI_SELECTORS.play)();
      }
    });
    navigator.mediaSession.setActionHandler("pause", () => {
      console.log("[mediasession] handling pause");
      if (isPlaying()) {
        clickOnUI(UI_SELECTORS.play)();
      }
    });
    navigator.mediaSession.setActionHandler(
      "nexttrack",
      clickOnUI(UI_SELECTORS.next)
    );
    navigator.mediaSession.setActionHandler(
      "previoustrack",
      clickOnUI(UI_SELECTORS.prev)
    );

    const ctrlsContainer = document.querySelector(
      UI_SELECTORS.controls_container
    );
    if (ctrlsContainer) {
      const observer = new MutationObserver(onTrackChange);
      observer.observe(ctrlsContainer, { subtree: true, childList: true });

      updateTrackMeta();
    }
  }

  function isPlaying() {
    const PLAY_CLASS = "player-controls__btn_pause";
    const playBtn = document.querySelector(UI_SELECTORS.play);

    if (!playBtn) {
      console.error("cannot find play button while checking plyaing status");
      return;
    }

    return playBtn.classList.contains(PLAY_CLASS);
  }

  function clickOnUI(sel) {
    return () => {
      const elm = document.querySelector(sel);
      if (!elm) {
        console.error(
          "cannot click on element. it doesnt exist in DOM. CSS selector:",
          sel
        );
        return;
      }
      elm.click();
    };
  }

  function updateTrackMeta() {
    try {
      const trName = document
        .querySelector(UI_SELECTORS.track_title)
        .textContent.trim();
      const trArtists = document
        .querySelector(UI_SELECTORS.track_artist)
        .textContent.trim();
      const trCover = document.querySelector(UI_SELECTORS.cover_image).src;

      navigator.mediaSession.metadata = new MediaMetadata({
        title: trName,
        artist: trArtists,
        artwork: [{ src: trCover }],
      });
    } catch (_err) {}
  }

  function onTrackChange(_mutations) {
    setTimeout(updateTrackMeta, 1000);
  }
})();
