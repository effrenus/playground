# Overview

Web version of Yandex.Music doesn't use MediaSession API, so there is no way to control player from BLE headphones.
This user script add basic support (play/stop, next/prev track) via MediaSession API.
