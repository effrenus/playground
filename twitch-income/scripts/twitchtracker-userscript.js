// ==UserScript==
// @name         New Userscript
// @namespace    http://tampermonkey.net/
// @version      0.1
// @match        https://twitchtracker.com/*
// @icon         https://www.google.com/s2/favicons?domain=twitchtracker.com
// @grant        none
// ==/UserScript==

(function () {
  const USERS = ["…"];
  const sleep = (ms = 100) => new Promise((resolv) => setTimeout(resolv, ms));

  async function main() {
    const cur_user = decodeURIComponent(
      window.location.pathname.split`/`.filter(Boolean).at(-1)
    );
    const idx = USERS.indexOf(cur_user.toLowerCase());
    console.log("user", cur_user.toLowerCase(), "idx", idx);

    try {
      const data = {
        username: cur_user,
        active_subs: parseFloat(
          document
            .querySelector(
              "div.g-x-wrapper:nth-child(7) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1)"
            )
            .textContent.replace(",", "")
            .replace(" ", "")
        ),
        gift_subs: parseFloat(
          document
            .querySelector(
              "div.g-x-wrapper:nth-child(7) > div:nth-child(1) > div:nth-child(2) > div:nth-child(3) > div:nth-child(1)"
            )
            .textContent.replace(",", "")
            .replace(" ", "")
        ),
        lang: document
          .querySelector(
            "li.list-group-item:nth-child(5) > div:nth-child(1) > div:nth-child(2) > span:nth-child(1)"
          )
          .textContent.toLowerCase(),
      };
      console.log("data:", data);

      const encoded = btoa(JSON.stringify(data));

      await fetch("http://localhost:8100/" + encoded);
    } catch (err) {}

    next(idx);
  }

  setTimeout(main, 2000);

  function next(idx) {
    if (idx === USERS.length - 1) {
      return;
    }

    window.location = `https://twitchtracker.com/${USERS[idx + 1]}`;
  }
})();
