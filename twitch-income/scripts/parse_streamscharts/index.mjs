import puppeteer from "puppeteer";

const sleep = (ms = 100) => new Promise((resolv) => setTimeout(resolv, ms));
const rand = (start, end) => start + Math.round(Math.random() * (end - start));

(async () => {
  const browser = await puppeteer.launch({
    headless: false,
  });

  const page = await browser.newPage();
  await page.setViewport({
    width: 1500,
    height: 1200,
  });
  await page.setExtraHTTPHeaders({
    "user-agent":
      "Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0",
    "X-CSRF-TOKEN": "",
    Cookie: "XSRF-TOKEN=",
    "X-Livewire": "true",
  });

  for (const login of ["domostanton", "karmikkoala"]) {
    await page.goto(`https://streamscharts.com/channels/${login}`);

    let i = 3;
    while (i--) {
      await sleep(10_000);
      const res = await page.evaluate(() => {
        const getVal = (elm) => {
          const val = elm.dataset.tippyContent
            ? elm.dataset.tippyContent
            : elm.textContent.trim();
          return parseFloat(val.replace(" ", ""));
        };

        const peak_view = document.querySelector(
          "li.justify-between:nth-child(3) > div:nth-child(2) > span:nth-child(2)"
        );
        const avg_view = document.querySelector(
          "li.justify-between:nth-child(4) > div:nth-child(2) > span:nth-child(1)"
        );
        const active_days = document.querySelector(
          "li.justify-between:nth-child(6) > div:nth-child(2) > span:nth-child(1)"
        );

        if (!peak_view) {
          return null;
        }

        return {
          peak_view: getVal(peak_view),
          avg_view: getVal(avg_view),
          active_days: getVal(active_days),
        };
      });

      if (res) {
        console.log(res);
        break;
      }
    }

    // await page.screenshot({ path: "example.png" });

    await sleep(3000 + rand(10, 800));
  }

  await browser.close();
})();
