const http = require("http");
const fs = require("fs");

const hostname = "127.0.0.1";
const port = 8100;

const server = http.createServer((req, res) => {
  const data = req.url.split("/").filter(Boolean).pop();
  const decoded = JSON.parse(Buffer.from(data, "base64").toString());

  fs.appendFileSync(
    "../data/more_data_streamelements.csv",
    Object.keys(decoded).map((k) => decoded[k]).join`,` + "\n"
  );

  res.statusCode = 200;
  res.setHeader("Content-Type", "text/plain");
  res.end("OK");
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
