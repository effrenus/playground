// ==UserScript==
// @name         New Userscript
// @namespace    http://tampermonkey.net/
// @version      0.1
// @author       You
// @match        https://streamscharts.com/channels/*
// @icon         https://www.google.com/s2/favicons?domain=streamscharts.com
// @grant        none
// ==/UserScript==

const USERS = ["…"];

const sleep = (ms = 100) => new Promise((resolv) => setTimeout(resolv, ms));

async function main(tries = 1) {
  const getVal = (elm) => {
    const val = elm.dataset.tippyContent
      ? elm.dataset.tippyContent
      : elm.textContent.trim();
    return parseFloat(val.replace(" ", ""));
  };

  const cur_user = decodeURIComponent(
    window.location.pathname.split`/`.filter(Boolean).pop()
  );
  const idx = USERS.indexOf(cur_user.toLowerCase());
  console.log("user", cur_user.toLowerCase(), "idx", idx);

  const peak_view = document.querySelector(
    "li.justify-between:nth-child(3) > div:nth-child(2) > span:nth-child(2)"
  );
  const avg_view = document.querySelector(
    "li.justify-between:nth-child(4) > div:nth-child(2) > span:nth-child(1)"
  );
  const active_days = document.querySelector(
    "li.justify-between:nth-child(6) > div:nth-child(2) > span:nth-child(1)"
  );
  const airtime = document.querySelector(
    "li.justify-between:nth-child(5) > div:nth-child(2) > span:nth-child(2)"
  );
  let status = document
    .querySelector("div.about-block__item:nth-child(1) > span:nth-child(2)")
    ?.textContent?.trim()
    ?.toLowerCase();
  let lang = document
    .querySelector("div.about-block__item:nth-child(2) > span:nth-child(2)")
    ?.textContent?.trim()
    .toLowerCase();

  if (!lang) {
    lang = status;
    status = "";
  }

  if (!peak_view) {
    if (!tries) {
      if (document.querySelector("main").textContent.includes("missing")) {
        next(idx);
      }
      return null;
    }
    console.log("Sleeping");
    await sleep(3000);
    return main(0);
  }

  const res = {
    user: cur_user,
    peak_view: getVal(peak_view),
    avg_view: getVal(avg_view),
    active_days: getVal(active_days),
    airtime: getVal(airtime),
    active_days: getVal(active_days),
    status,
    lang,
  };
  console.log("res", res);

  try {
    await fetch("http://localhost:8100/" + btoa(JSON.stringify(res)));
  } catch (err) {}

  next(idx);
}

function next(idx) {
  if (idx === USERS.length - 1) {
    return;
  }

  window.location = `https://streamscharts.com/channels/${USERS[idx + 1]}`;
}

setTimeout(main, 3851);
