// ==UserScript==
// @name         New Userscript
// @namespace    http://tampermonkey.net/
// @version      0.1
// @match        https://stats.streamelements.com/c/*
// @icon         https://www.google.com/s2/favicons?domain=streamelements.com
// @grant        none
// ==/UserScript==

const USERS = ["…"];

const sleep = (ms = 100) => new Promise((resolv) => setTimeout(resolv, ms));

async function main(tries = 1) {
  const cur_user = decodeURIComponent(
    window.location.pathname.split`/`.filter(Boolean).pop()
  );
  const idx = USERS.indexOf(cur_user.toLowerCase());
  console.log("user", cur_user.toLowerCase(), "idx", idx);

  const chat_stats = document.querySelector(
    "div.c0156:nth-child(1) > div:nth-child(2) > div:nth-child(2)"
  );

  if (!chat_stats) {
    if (!tries) {
      return next(idx);
    }

    console.log("Sleeping");
    await sleep(3000);

    return main(0);
  }

  let bots_stats;
  try {
    bots_stats = Array(10)
      .fill("")
      .map((_, i) => [
        document.querySelector(
          `div.c0172:nth-child(3) > div:nth-child(3) > div:nth-child(${
            i + 1
          }) > div:nth-child(3)`
        ).textContent,
        +document
          .querySelector(
            `div.c0172:nth-child(3) > div:nth-child(3) > div:nth-child(${
              i + 1
            }) > div:nth-child(4)`
          )
          .textContent.trim()
          .replace(/\s+/g, "")
          .replace(/,/g, ""),
      ])
      .filter(
        ([name]) =>
          name.includes("bot") ||
          name.includes("b0t") ||
          ["streamelements", "streamlabs", cur_user].includes(name)
      )
      .reduce((acc, [_, n]) => acc + n, 0);
    console.log("bot stats", bots_stats);
  } catch (err) {
    await sleep(3000);
    return main(0);
  }

  const res = {
    user: cur_user,
    chat_stats:
      chat_stats.textContent.trim().replace(/\s+/g, "").replace(/,/g, "") -
      bots_stats,
  };
  console.log("res", res);
  await sleep(1200);

  try {
    await fetch("http://localhost:8100/" + btoa(JSON.stringify(res)));
  } catch (err) {}

  next(idx);
}

function next(idx) {
  if (idx === USERS.length - 1) {
    return;
  }

  window.location = `https://stats.streamelements.com/c/${USERS[idx + 1]}`;
}

setTimeout(main, 2050);
