import fs from "fs";

const source1 = fs.readFileSync("../data/full_stats.csv").toString()
  .split`\n`.map((r) => r.split`,`);
const source2 = fs
  .readFileSync("../data/more_data_streamscharts.csv")
  .toString().split`\n`.map((r) => r.split`,`);

const res = [];

for (const r1 of source1.slice(1)) {
  const additional_data = source2
    .slice(1)
    .find((r2) => r1[2].toLowerCase() === r2[0].toLowerCase());

  if (!additional_data) {
    continue;
  }

  res.push([...r1.slice(1), ...additional_data.slice(1)]);
}

res.unshift([...source1[0].slice(1), ...source2[0].slice(1)]);

fs.writeFileSync(
  "../data/combined_stats_2.csv",
  res.map((r) => r.join`,`).join`\n`
);
