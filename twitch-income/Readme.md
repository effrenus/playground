# Twitch streamers income EDA

[Preview](https://nbviewer.ipython.org/urls/bitbucket.org/effrenus/playground/raw/e22dcd6fcd8c661b6375c72b51546f328b7087af/twitch-income/income-eda.ipynb)

## Sources of data

- https://streamscharts.com/channels/
- https://stats.streamelements.com/
- https://twitchtracker.com/
- https://api.twitch.tv/

## TODO

- Redo all work
- Plots pretty bad, needs improve
- Try Ridge, Elastic, Lasso regressions
