import { ensureDir } from 'https://deno.land/std/fs/mod.ts'
import * as log from 'https://deno.land/std/log/mod.ts'
import { assert } from 'https://deno.land/std/testing/asserts.ts'
import { resolve, dirname, basename } from 'https://deno.land/std/path/mod.ts'

await log.setup({
  handlers: {
    console: new log.handlers.ConsoleHandler('DEBUG'),
  },
  loggers: {
    default: {
      level: 'DEBUG',
      handlers: ['console'],
    }
  }
})

const logger = log.getLogger()

interface HAREntry {
  request: { url: string }
  response: {
    content: {
      text: string
    }
  }
}

/** Full specification https://github.com/ahmadnassri/har-spec/blob/master/versions/1.2.md */
interface HARLog {
  log: {
    version: string
    entries: HAREntry[]
  }
}

async function readEntriesFromHAR(input: Deno.Reader): Promise<HAREntry[]> {
  const buf = new Deno.Buffer
  await buf.readFrom(input)

  const har = JSON.parse(new TextDecoder().decode(buf.bytes())) as HARLog

  assert(/1(\.?|$)/.test(har.log?.version || ''), `Supporting versions 1.x, but got <${har.log?.version}>`)

  return har.log?.entries || []
}

async function getSourceMapContent(url: string) {
  const sourceMapUrl = new URL(url)
  const res = await fetch(sourceMapUrl.href)
  if (!res.ok || res.status !== 200) throw new Error(`Remote sourcemap doesn't exists: response status: ${res.status}`)
  return await res.json()
}

async function resolveAbsoluteSourceMapUrl({ request, response }: HAREntry) {
  const scriptUrl = request.url
  const scriptContent = response.content.text

  logger.debug(`resolving sourcemap url for ${scriptUrl}`)

  let sourceMapUrl = null
  if (/\/\/[#@]\s+sourceMappingURL\s*=\s*.+$/m.test(scriptContent)) {
    // Checking `//# sourceMappingURL=<url>` at the end of script body.
    [, sourceMapUrl] = scriptContent.match(/\/\/[#@]\s+sourceMappingURL\s*=\s*(.+)$/m) ?? []
    if (sourceMapUrl && !sourceMapUrl.startsWith('http')) {
      sourceMapUrl = new URL(sourceMapUrl, scriptUrl).href
    }
  } else if (false) {
    // TODO: Check in headers.
    // `SourceMap:` or `X-SourceMap:` (deprecated)
  } else {
    // Append `.map` suffix and send `HEAD` request to check existence.
    const res = await fetch(`${scriptUrl}.map`, { method: 'HEAD' })
    if (res.ok && res.status === 200) sourceMapUrl = `${scriptUrl}.map`
  }

  logger.debug(`resolved url: <${sourceMapUrl}>`)

  return sourceMapUrl
}

async function tryFetchRemote({ url, base }: { url: string | undefined, base: string | undefined}) {
  if (!url) return null
  try {
    url = new URL(url, base).href
    logger.debug('fetching remote sourcemap', url)
    return await (await fetch(url)).text()
  } catch (err) {
    logger.error('unable to fetch remote sourcemap', err)
  }
  return null
}

async function restoreSourcesStruct(url: string, { outDir }: { outDir: string }) {
  const sourcemap = await getSourceMapContent(url)

  for (let i = 0; i < sourcemap.sourcesContent.length; ++i) {
    let content = sourcemap.sourcesContent[i] || await tryFetchRemote({ url: sourcemap?.sources[i], base: sourcemap?.sourceRoot })

    if (!content) continue
  
    const path = (sourcemap.sources[i] || '').replace('webpack:///', '')
    const [dirs, filename] = [resolve(outDir, dirname(path)), basename(path)]
  
    await ensureDir(dirs)
  
    await Deno.writeFile(resolve(dirs, filename), new TextEncoder().encode(content))
  }
}

async function main() {
  const outDir = Deno.args[0] || 'output_sources'
  const entries = await readEntriesFromHAR(Deno.stdin)

  logger.debug(`found ${entries.length} HAR entries`)

  for (const harEntry of entries) {
    try {
      const sourceMapUrl = await resolveAbsoluteSourceMapUrl(harEntry)
      if (!sourceMapUrl) continue
      await restoreSourcesStruct(sourceMapUrl, { outDir })
    } catch (err) {
      logger.error(`cannot get map for <${harEntry.request.url}>: ${err}`)
    }
  }
}

main()
