# Retrieving original JS files from sourcemaps

Script trying to find sourcemaps and create local copy of original file from sourcemap. You must send to `stdin` HAR records.

## Usage

* Install Deno – [https://deno.land/#installation](https://deno.land/#installation) 👍🏻.
* [Copy HAR](https://developer.mozilla.org/en-US/docs/Tools/Network_Monitor/request_list#Context_menu) for JS files from devtools panel.
* Run `cat js_files.har | deno run --allow-all --unstable ./index.ts <outputDir>`.

## Specifications

* [Source Map](https://docs.google.com/document/d/1U1RGAehQwRypUTovF1KRlpiOFze0b-_2gc6fAH0KY0k/edit#heading=h.djovrt4kdvga) [PDF] / Mirror: https://sourcemaps.info/spec.html
* HAR – [https://github.com/ahmadnassri/har-spec/blob/master/versions/1.2.md](https://github.com/ahmadnassri/har-spec/blob/master/versions/1.2.md)