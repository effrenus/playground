# Quizes

```js
(100)["toString"]["length"]; // Wrap up primitive, return `toString` method, get formal parameters length of the method.
```

```js
var a = (1, 5 - 1) * 2; // (1, 5-1) * 2 -> 8
```

```js
function f(x, y) {
  x = 10;
  console.log(
    arguments[0], // undefined. In sloppy mode mirroring works in one way: arguments -> formal parameters.
    arguments[1] // undefined
  );
}

f();
```

```js
var b = 10,
  c =
    (20, // 1
    function (x) {
      return x + 100;
    }, // 2
    function () {
      return arguments[0];
    }); // 3 // `,` operator returns right operand 3.

a =
  b +
  c(
    // JS doesn't insert automatically semicolon (ASI) here, because next token is valid by grammar.
    { x: 10 }
  ).x;
```

```js
const obj = {
  a: 1,
  b() {
    return this.a;
  },
};
console.log(obj.b()); //=> 1. `.` returns Reference Record, this binded to `obj`.
console.log((true ? obj.b : a)()); //=> undefined. Conditional operator, returns value (https://tc39.es/ecma262/multipage/ecmascript-language-expressions.html#sec-conditional-operator-runtime-semantics-evaluation).
console.log((true, obj.b)()); //=> undefined. Comma operator, returns value (https://tc39.es/ecma262/multipage/ecmascript-language-expressions.html#sec-runtime-semantics-keyeddestructuringassignmentevaluation).
console.log((3, obj["b"])()); //=> undefined. Comma operator, returns value.
console.log(obj.b()); //=> 1. Grouping operator, returns Reference Record (https://tc39.es/ecma262/multipage/ecmascript-language-expressions.html#sec-grouping-operator-runtime-semantics-evaluation).
console.log((obj.c = obj.b)()); //=> undefined. Assignment operator, returns value (https://tc39.es/ecma262/multipage/ecmascript-language-expressions.html#sec-assignment-operators-runtime-semantics-evaluation).
```

Sources:

- [http://dmitrysoshnikov.com/ecmascript/the-quiz/](http://dmitrysoshnikov.com/ecmascript/the-quiz/)
- [http://dmitry.baranovskiy.com/post/91403200](http://dmitry.baranovskiy.com/post/91403200)
- [http://perfectionkills.com/javascript-quiz/](http://perfectionkills.com/javascript-quiz/)
- [https://humanwhocodes.com/blog/2010/02/16/my-javascript-quiz/](https://humanwhocodes.com/blog/2010/02/16/my-javascript-quiz/)
- [https://bigfrontend.dev/quiz](https://bigfrontend.dev/quiz)
