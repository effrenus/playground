const CHECK_INTERVAL = 5000 // ms

let isRunning = false
let timerId = null

let prevState = null
let isDevPanelOpened = false

const img = new Image
Object.defineProperty(img, 'id', {
    get() {
        isDevPanelOpened = true
        return null
    }
})

function emitChange() {
    document.dispatchEvent(new CustomEvent('devpanel', {
        detail: { isDevPanelOpened }
    }))
}

function start(opts = {}) {
    if (isRunning) return

    isRunning = true
    const defaultOpts = {
        clear: true
    }
    opts = Object.assign({}, defaultOpts, opts)
    
    function detect() {
        isDevPanelOpened = false
        console.dir(img)
        if (opts.clear) console.clear()
        if (prevState !== isDevPanelOpened) {
            emitChange()
        }
        prevState = isDevPanelOpened
        timerId = setTimeout(detect, CHECK_INTERVAL)
    }

    detect()
}

start.stop = () => {
    clearTimeout(timerId)
    isRunning = false
}

export default start
